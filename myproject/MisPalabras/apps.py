from xml.sax.handler import ContentHandler
from xml.sax import make_parser

#Manejamos la clase wikipedia, recibe los elementos de la wikipedia y actua en función a ellos
class WikipediaHandler(ContentHandler): 

    def __init__ (self):
        self.inContent = False
        self.content = ""

    # Función para extraer las palabras que queramos de wikipedia
    def startElement (self, name, attrs):
        if name == 'extract':
            self.inContent = True

    # Función para crear la definición de la palabra de wikipedia
    def endElement (self, name):
        global significado

        if name == 'extract':
            significado ="Significado: " + self.content + "."
        if self.inContent:
            self.inContent = False
            self.content = ""

    # Función que añade a nuestro content que lo hemos inicializado vacio le añadimos las letras de las palabras
    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class Wiki:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = WikipediaHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    # Después de inicializar lo que necesitamos para python creamos la función con la variable goblara definición
    def significado(self):
        global significado

        return significado


class FlickrImagenHandler(ContentHandler):

    def __init__ (self):
        self.inEntry = False
        self.inFeed = False
        self.inContent = False
        self.content = ""
        self.link = ""

    # feed, entry, rel (que siempre tiene que ser igual a enclosure) y href es del propio flickr cuando vamos a buscar una imagen 
    # Función que recoge un elemento de flickr
    def startElement(self, name, attrs):
        if name == 'feed':
            self.inFeed = True
        elif self.inFeed:
            if name == 'entry':
                self.inEntry = True
            elif self.inEntry:
                if attrs.get('rel') == 'enclosure':
                    self.link = attrs.get('href')

    # Función para crear el link de nuestra palabra
    def endElement(self, name):
        global enlazar

        if name == 'entry':
            self.inEntry = False
            self.inFeed = False
            enlazar = self.link

class FlickrImagen:
    # Aquí llamamos al manejador (Handler) que hemos ido creando

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = FlickrImagenHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def desc_enlace(self):
        global enlazar

        return enlazar