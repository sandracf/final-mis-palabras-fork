import urllib.request

from django.db import migrations, models

from ..apps import Wiki

def desc_list_words(apps, schema_editor):
    Pag = apps.get_model('MisPalabras', 'Pag')
    list_words = ['meme', 'alimento', 'caramelo', 'casa','libro', 'pelota', 'pez', 'lago', 'pizza', 'playa', 'raqueta']
    for word in list_words:
        url = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles='+ word + \
          '&prop=extracts&exintro&explaintext'
        xmlStream = urllib.request.urlopen(url)
        significado = Wiki(xmlStream).significado()
        p = Pag(name=word, content=significado, selected=False)
        p.save()

class Migration(migrations.Migration):

    dependencies = [
        ('MisPalabras', '0001_initial'),
  
	]
    operations = [
        migrations.RunPython(desc_list_words)
        
    ]
